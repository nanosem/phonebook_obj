//
//  MainViewCell.h
//  PhoneBookApp_objc
//
//  Created by Sem Vastuin on 16.03.17.
//  Copyright © 2017 brander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photoImageBox;
@property (weak, nonatomic) IBOutlet UILabel *fullNameLabel;

- (void)setCellUserName:(NSString*)fullName andPhoto:(UIImage*)photo;

@end
