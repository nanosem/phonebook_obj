//
//  MainViewCell.m
//  PhoneBookApp_objc
//
//  Created by Sem Vastuin on 16.03.17.
//  Copyright © 2017 brander. All rights reserved.
//

#import "MainViewCell.h"

@implementation MainViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellUserName:(NSString*)fullName andPhoto:(UIImage*)photo {
    self.fullNameLabel.text = fullName;
    self.photoImageBox.image = photo;
}

@end
