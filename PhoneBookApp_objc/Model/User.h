//
//  User.h
//  PhoneBookApp_objc
//
//  Created by Sem Vastuin on 15.03.17.
//  Copyright © 2017 brander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface User : NSObject <NSCopying>

@property (strong, nonatomic) UIImage*  image;
@property (strong, nonatomic) NSString* identifier;
@property (strong, nonatomic) NSString* firstName;
@property (strong, nonatomic) NSString* lastName;
@property (strong, nonatomic) NSString* phoneNumber;
@property (strong, nonatomic) NSString* email;


+(NSInteger) instanceCounter;


//-(id) copyWithZone:(NSZone *)zone;
@end
