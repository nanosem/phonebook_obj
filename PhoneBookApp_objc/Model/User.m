//
//  User.m
//  PhoneBookApp_objc
//
//  Created by Sem Vastuin on 15.03.17.
//  Copyright © 2017 brander. All rights reserved.
//

#import "User.h"

static NSInteger instanceCount = 0;

@implementation User

- (instancetype)init {
    self = [super init];
    if(self) {
        self.identifier = [NSString stringWithFormat:@"%ld", (long)instanceCount];
        instanceCount++;
    }
    return self;
}

+ (NSInteger)instanceCounter {
    return instanceCount;
}

-(id)copyWithZone:(NSZone *)zone {
    User *user = [[User allocWithZone:zone] init];
    
    user->_identifier = [self identifier];
    user->_image = [self image];
    user->_firstName = [self firstName];
    user->_lastName = [self lastName];
    user->_phoneNumber = [self phoneNumber];
    user->_email = [self email];
    
    return user;
}

@end
