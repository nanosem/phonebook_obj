//
//  DataManager.h
//  PhoneBookApp_objc
//
//  Created by Sem Vastuin on 15.03.17.
//  Copyright © 2017 brander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface DataManager : NSObject

@property (strong, nonatomic, readonly) NSMutableDictionary* userList;
@property (strong, nonatomic, readonly) NSMutableArray* sectionHeaders;

// MARK: - Singleton
+ (DataManager*) instance;

// MARK: - DataManager methods
- (void) addUser:(User*) user;
- (void) editUser:(User* ) user;
- (void) deleteUser:(User* ) user;

@end
