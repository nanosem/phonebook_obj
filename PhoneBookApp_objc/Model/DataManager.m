//
//  DataManager.m
//  PhoneBookApp_objc
//
//  Created by Sem Vastuin on 15.03.17.
//  Copyright © 2017 brander. All rights reserved.
//

#import "DataManager.h"
#import "User.h"

@interface DataManager()
    @property (strong, nonatomic) NSMutableDictionary* userList;
    @property (strong, nonatomic) NSMutableArray* sectionHeaders;
@end

@implementation DataManager

// MARK: - Singleton
static DataManager *instance_ = nil;

+ (DataManager *) instance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance_ = [[DataManager alloc] init];
    });
    
    return instance_;
}

-(id) init {
    self = [super init];
    
    if (self) {
        self.sectionHeaders = [[NSMutableArray alloc] init];
        self.userList = [[NSMutableDictionary alloc] init];
    }
    return self;
}

// MARK: - DataManager methods
- (void) addUser:(User*) user {
    NSString *currentKey = [[user firstName] substringToIndex:1];
    if (![self.sectionHeaders containsObject:currentKey]) {
        [[self sectionHeaders] addObject: currentKey];
        
        NSMutableArray *usersForCurrentKey = [[NSMutableArray alloc] initWithObjects:user, nil];
        
        [[self userList] setObject:usersForCurrentKey forKey:currentKey];
    } else {
        NSMutableArray *usersForCurrentKey = [[NSMutableArray alloc] init];
        
        usersForCurrentKey = [[self userList] objectForKey:currentKey];
        [usersForCurrentKey addObject: user];
        
        [[self userList] setObject:usersForCurrentKey forKey:currentKey];
    }
    
    //Keeping data sorted
    [[self sectionHeaders] sortUsingComparator:^NSComparisonResult(NSString *header1, NSString *header2) {
        return [header1 compare:header2];
    }];
    
    [[[self userList] objectForKey:currentKey] sortUsingComparator:^NSComparisonResult(User *user1, User *user2) {
        return [user1.firstName compare:user2.firstName options:NSCaseInsensitiveSearch];
    }];
    
    //Post notification
    [[NSNotificationCenter defaultCenter] postNotificationName:@"userAdded" object:nil];
}

- (void) editUser:(User* ) user {
    NSString *key = [[user firstName] substringToIndex:1];
    
    User *oldUser;
    for (NSString *currentKey in [self sectionHeaders]) {
        for (User *currentUser in [[self userList] objectForKey:currentKey]) {
            if (currentUser.identifier == user.identifier) {
                oldUser = currentUser;
            }
        }
    }
    
    NSString *oldKey = [oldUser.firstName substringToIndex:1];
    
    if (key == oldKey) {
        NSUInteger indexOfUser = [[[self userList] objectForKey:key] indexOfObject:oldUser];
        [[[self userList] objectForKey: key] replaceObjectAtIndex:indexOfUser withObject: user];
    } else {
        [self deleteUser:oldUser];
        [self addUser:user];
    }
    
    //Keeping data sorted
    [[self sectionHeaders] sortUsingComparator:^NSComparisonResult(NSString *header1, NSString *header2) {
        return [header1 compare:header2];
    }];
    
    [[[self userList] objectForKey:key] sortUsingComparator:^NSComparisonResult(User *user1, User *user2) {
        return [user1.firstName compare:user2.firstName options:NSCaseInsensitiveSearch];
    }];
    
    //Post notification
    [[NSNotificationCenter defaultCenter] postNotificationName:@"userEdited" object:nil];
}

- (void) deleteUser:(User* ) user {
    
    NSString *currentKey = [[user firstName] substringToIndex:1];
    [[[self userList] objectForKey:currentKey] removeObject: user];
    
    if ([[[self userList] objectForKey:currentKey] count] == 0) {
        [[self userList] setValue:nil forKey:currentKey];
        [[self sectionHeaders] removeObject:currentKey];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"userDeleted" object:nil];
}

@end
