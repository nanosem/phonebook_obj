//
//  MainTableViewController.h
//  PhoneBookApp_objc
//
//  Created by Sem Vastuin on 15.03.17.
//  Copyright © 2017 brander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController: UITableViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@end
