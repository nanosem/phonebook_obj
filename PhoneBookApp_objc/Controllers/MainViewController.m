//
//  MainTableViewController.m
//  PhoneBookApp_objc
//
//  Created by Sem Vastuin on 15.03.17.
//  Copyright © 2017 brander. All rights reserved.
//

#import "MainViewController.h"
#import "EditUserViewController.h"
#import "MainViewCell.h"
#import "DataManager.h"

@interface MainViewController()

@property (strong, nonatomic) NSMutableArray *filteredUsers;
@property (weak, nonatomic) UISearchBar *searchBar;
@end
@implementation MainViewController

// MARK: - UIView
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.filteredUsers = [[NSMutableArray alloc] init];
    
    //Notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userEdited)
                                                 name:@"userEdited"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userAdded)
                                                 name:@"userAdded"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userDeleted)
                                                 name:@"userDeleted"
                                               object:nil];
    
    
    //Configurating tableViewCell
    UINib* cellNib = [UINib nibWithNibName:NSStringFromClass([MainViewCell class]) bundle:nil];
    [[self tableView] registerNib:cellNib forCellReuseIdentifier:NSStringFromClass([MainViewCell class])];
    
    //NavigationBar buttons
    UIImage *showUserInfoButtonImage = [UIImage imageNamed:@"add.png"];
    UIBarButtonItem *showUserInfoButton = [[UIBarButtonItem alloc] initWithImage:showUserInfoButtonImage
                                                                           style:UIBarButtonItemStylePlain
                                                                          target:self
                                                                          action:@selector(showEditUserView)];

    //SearchController
    UISearchController *searchController = [[UISearchController alloc] initWithSearchResultsController:self];
    self.searchBar = searchController.searchBar;

    //NavigationBar items
    self.navigationItem.rightBarButtonItem = showUserInfoButton;
    self.navigationItem.titleView = searchController.searchBar;
    
    //NavigationBar configuration
    self.navigationController.navigationBar.translucent = NO;
    self.definesPresentationContext = YES;
    self.navigationController.navigationBarHidden = NO;
    
    //Configurating navigationBar color
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor= [UIColor colorWithRed:0.65 green:0.14 blue:0.68 alpha:1.0];
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.searchBar.delegate = self;
}

// MARK: - TableView dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([self.filteredUsers count] == 0) {
        NSUInteger numberOfSections = [[[DataManager instance] sectionHeaders] count];
        return numberOfSections;
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self.filteredUsers count] == 0) {
        NSString *currentKey = [[[DataManager instance] sectionHeaders] objectAtIndex:section];
        NSUInteger numberOfElementsInSection = [[[[DataManager instance] userList] objectForKey:currentKey] count];
        return numberOfElementsInSection;
    } else {
        return [[self filteredUsers] count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MainViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MainViewCell class]) forIndexPath:indexPath];
    User *user;
    if ([self.filteredUsers count] == 0) {
        NSString *currentKey = [[[DataManager instance] sectionHeaders] objectAtIndex: [indexPath section]];
        user = [[[[DataManager instance] userList] objectForKey: currentKey] objectAtIndex: [indexPath row]];
    } else {
        user = [[self filteredUsers] objectAtIndex:[indexPath row]];
    }
    
    NSString *fullName = [NSString stringWithFormat:@"%@ %@", [user firstName], [user lastName]];
    [cell setCellUserName: fullName andPhoto: [user image]];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        User *user;
        NSString *currentKey;
        if ([self.filteredUsers count] == 0) {
            currentKey = [[[DataManager instance] sectionHeaders] objectAtIndex: [indexPath section]];
            user = [[[[DataManager instance] userList] objectForKey: currentKey] objectAtIndex: [indexPath row]];
            
        } else {
            user = [[self filteredUsers] objectAtIndex:[indexPath row]];
            currentKey = [[user firstName] substringToIndex:1];
        }
        
        [[DataManager instance] deleteUser: user];
        if ([self.searchBar.text length] == 0) {
            if ([[[DataManager instance] sectionHeaders] containsObject:currentKey]) {
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            } else {
                NSIndexSet *indexSet = [[NSIndexSet alloc] initWithIndex:[indexPath section]];
                [tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
            }
        }
    }
}

// MARK: - TableVeiw delegate
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [[[DataManager instance] sectionHeaders] objectAtIndex: section];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([self.filteredUsers count] == 0) {
        return 25;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    EditUserViewController *editUserViewController = [[EditUserViewController alloc] initWithNibName:@"EditUserViewController" bundle:nil];
    
    NSString *currentKey;
    User* currentUser;
    if ([[self filteredUsers] count] == 0 ) {
        currentKey = [[[DataManager instance] sectionHeaders] objectAtIndex:[indexPath section]];
        currentUser = [[[[DataManager instance] userList] objectForKey:currentKey] objectAtIndex:[indexPath row]];
    } else {
        currentUser = [[self filteredUsers] objectAtIndex:[indexPath row]];
    }
    [editUserViewController setUser:currentUser];
    
    [self.navigationController pushViewController:editUserViewController animated:YES];
}

// MARK: - SearchBar delegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [[self filteredUsers] removeAllObjects];
    for (NSString *currentKey in [[DataManager instance] sectionHeaders]) {
        for (User *user in [[[DataManager instance] userList] objectForKey:currentKey]) {
            if ([[user.firstName lowercaseString] containsString:[searchText lowercaseString]]) {
                [[self filteredUsers] addObject:user];
            }
        }
    }
    [self.tableView reloadData];
}

// MARK: - Adittional methods
- (void) showEditUserView {
    EditUserViewController *editUserViewController = [[EditUserViewController alloc] initWithNibName:@"EditUserViewController" bundle:nil];
    [self.navigationController pushViewController:editUserViewController animated:YES];
}

- (void) userAdded {
    if ([self.searchBar.text length] != 0) {
        for (NSString *key in [[DataManager instance] sectionHeaders]) {
            for (User *user in [[[DataManager instance] userList] objectForKey:key]) {
                if ([user.firstName containsString:[[self searchBar] text]]) {
                    [self.filteredUsers addObject:user];
                }
            }
        }
    }
    [self.tableView reloadData];
}

- (void) userEdited {
    if ([self.searchBar.text length] != 0) {
        for (NSString *key in [[DataManager instance] sectionHeaders]) {
            for (User *user in [[[DataManager instance] userList] objectForKey:key]) {
                if ([[self filteredUsers] containsObject: user]) {
                    if (![user.firstName containsString: self.searchBar.text]) {
                        [self.filteredUsers removeObject:user];
                    }
                }
            }
        }
    }
    [self.tableView reloadData];
}

- (void) userDeleted {
    if ([self.searchBar.text length] != 0) {
        for (NSString *key in [[DataManager instance] sectionHeaders]) {
            for (User *user in [[[DataManager instance] userList] objectForKey:key]) {
                if ([[self filteredUsers] containsObject: user]) {
                    if ([user.firstName containsString: self.searchBar.text]) {
                        [self.filteredUsers addObject:user];
                    }
                }
            }
        }
    }
    [self.tableView reloadData];
}
@end
