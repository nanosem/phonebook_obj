//
//  EditUserViewController.m
//  PhoneBookApp_objc
//
//  Created by Sem Vastuin on 16.03.17.
//  Copyright © 2017 brander. All rights reserved.
//

#import "EditUserViewController.h"
#import "DataManager.h"
#import "User.h"

@interface EditUserViewController()

@property (weak, nonatomic) IBOutlet UIImageView *photoImageBox;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextBox;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextBox;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextBox;
@property (weak, nonatomic) IBOutlet UITextField *emailTextBox;

@end

@implementation EditUserViewController {
    UIImagePickerController *imagePicker;
}

// MARK: - UIViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //ImagePicker
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    //NavigationController configurating
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBarHidden = NO;
    
    //Creating buttons for navigation bar
    UIBarButtonItem *saveUserButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(saveButtonPressed)];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(cancelButtonPressed)];
    [self.navigationItem setRightBarButtonItem: saveUserButton];
    [self.navigationItem setLeftBarButtonItem: cancelButton];
    
    
    //Setting user data to fields if it exist
    if (self.user != nil) {
        [self.firstNameTextBox setText:[self.user firstName]];
        [self.lastNameTextBox setText:[self.user lastName]];
        [self.phoneNumberTextBox setText:[self.user phoneNumber]];
        [self.emailTextBox setText:[self.user email]];
        [self.photoImageBox setImage:[self.user image]];
    } else {
        [self.photoImageBox setImage:[UIImage imageNamed:@"profile.jpg"]];
    }
}

// MARK: - ImagePicker
- (IBAction)selectPhoto:(id)sender {
    [self presentModalViewController:imagePicker animated:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
    [[self photoImageBox] setImage:image];
    [self dismissModalViewControllerAnimated:YES];
}

// MARK: - Additional methods
- (void) saveButtonPressed {

    if (self.user == nil) {
        if (self.isValidationDone) {
            User *currentUser = [[User alloc] init];
            currentUser.firstName =  [NSString stringWithFormat: @"%@", self.firstNameTextBox.text];
            currentUser.lastName = [NSString stringWithFormat: @"%@", self.lastNameTextBox.text];
            currentUser.phoneNumber = [NSString stringWithFormat: @"%@", self.phoneNumberTextBox.text];
            currentUser.email = [NSString stringWithFormat: @"%@", self.emailTextBox.text];
            currentUser.image = [self.photoImageBox image];
        
            [[DataManager instance] addUser: currentUser];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"Enter name and surname" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            return;
        }
    } else {
        User *editedUser = self.user;
        
        editedUser.firstName =  [NSString stringWithFormat: @"%@", self.firstNameTextBox.text];
        editedUser.lastName = [NSString stringWithFormat: @"%@", self.lastNameTextBox.text];
        editedUser.phoneNumber = [NSString stringWithFormat: @"%@", self.phoneNumberTextBox.text];
        editedUser.email = [NSString stringWithFormat: @"%@", self.emailTextBox.text];
        editedUser.image = [self.photoImageBox image];
        
        [[DataManager instance] editUser:editedUser];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) cancelButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL) isValidationDone {
    if ([self.firstNameTextBox hasText] && [self.lastNameTextBox hasText]) {
        return true;
    }
    return false;
}
@end
