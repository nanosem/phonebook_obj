//
//  EditUserViewController.h
//  PhoneBookApp_objc
//
//  Created by Sem Vastuin on 16.03.17.
//  Copyright © 2017 brander. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface EditUserViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (copy, nonatomic)  User *user;

@end
